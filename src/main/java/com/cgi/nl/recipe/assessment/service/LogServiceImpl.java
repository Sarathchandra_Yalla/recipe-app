package com.cgi.nl.recipe.assessment.service;

import com.cgi.nl.recipe.assessment.util.ReadLogFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@Service
public class LogServiceImpl implements LogService {

    public Map<String, Integer> filterLogFile(String logLevel)  {
        LinkedHashMap<String, Integer> reverseSortedMap = new LinkedHashMap<>();

        Map<String, Integer> filteredLogMap = ReadLogFile.readLogFile(logLevel);

        filteredLogMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(record -> reverseSortedMap.put(record.getKey(), record.getValue()));

        log.info("Reverse Sorted Map   : " + reverseSortedMap);
        return reverseSortedMap;
    }
}
