package com.cgi.nl.recipe.assessment.util;

import com.cgi.nl.recipe.assessment.domain.Recipe;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Slf4j
public class ReadJsonFile {

    public static List<Recipe> getRecipeDetails(){
        List<Recipe> recipesList = null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            File resource1 = new ClassPathResource("receipe.json").getFile();
            String text = new String(Files.readAllBytes(resource1.toPath()));

            Recipe[] recipeDetails = mapper.readValue(text,  Recipe[].class );
            log.info("Size of Recipes List: "+ recipeDetails.length);

            recipesList = Arrays.asList(recipeDetails);
            recipesList.sort(Comparator.comparing(Recipe::getTitle));
        }catch (Exception e){
            e.printStackTrace();
        }
        return recipesList;
    }
}
