package com.cgi.nl.recipe.assessment.controller;

import com.cgi.nl.recipe.assessment.domain.Recipe;
import com.cgi.nl.recipe.assessment.service.RecipeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/cgi/v1/recipe")
public class RecipeController {

    private RecipeService recipeService;

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Recipe>> getAllReceipes(){
        log.info("getAllReceipes of RecipeController");
        List<Recipe> allRecipes = recipeService.getAllRecipes();
        return ResponseEntity.ok(allRecipes);
    }

    @GetMapping(value = "/ingredients/{ingredients}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Recipe>> getAllIngredients(@PathVariable(value = "ingredients") String[] listOfIngredients){
        log.info("getAllIngredients of RecipeController");
        log.info("List of ingredients: "+listOfIngredients.toString());
        List<Recipe> allIngΩredients = recipeService.getAllIngredients(listOfIngredients);
        return ResponseEntity.ok(allIngΩredients);
    }
}
