package com.cgi.nl.recipe.assessment.service;

import com.cgi.nl.recipe.assessment.domain.Recipe;
import com.cgi.nl.recipe.assessment.util.ReadJsonFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class RecipeServiceImpl implements RecipeService {

    @Override
    public List<Recipe> getAllRecipes() {
        return ReadJsonFile.getRecipeDetails();
    }

    @Override
    public List<Recipe> getAllIngredients(String[] ingredientsInput) {
        List<Recipe> finalRecipesList = new ArrayList<>();

        List<Recipe> recipesList = ReadJsonFile.getRecipeDetails();
        for (Recipe recipe : recipesList) {
            if(isIngredientExistInRecipes(recipe.getIngredients(), ingredientsInput)){
                finalRecipesList.add(recipe);
            }
        }
        log.info("All ingredients recipes list size: "+finalRecipesList.size());
        return finalRecipesList;
    }

    private boolean isIngredientExistInRecipes(List<String> recipeIngredients, String[] ingredientsInput){
        int count=0;
        for(String str : ingredientsInput){
            if(recipeIngredients.contains(str))
                count++;
        }
        if(count == ingredientsInput.length)
            return true;
        return false;
    }
}
