package com.cgi.nl.recipe.assessment.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("recipe-api")
                .apiInfo(apiInfo()).select().paths(regex("/api/cgi/v1/recipe.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Multiple Recipes API")
                .description("Recipe API reference for CGI Tech Assessment")
                .contact("Sarathchandra.Yalla@gmail.com").license("Open Source License")
                .licenseUrl("Sarathchandra.Yalla@gmail.com").version("1.0").build();
    }
}
