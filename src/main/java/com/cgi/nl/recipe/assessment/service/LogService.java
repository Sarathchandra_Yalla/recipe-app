package com.cgi.nl.recipe.assessment.service;

import java.util.Map;

public interface LogService {
    Map<String, Integer> filterLogFile(String logLevel);
}
