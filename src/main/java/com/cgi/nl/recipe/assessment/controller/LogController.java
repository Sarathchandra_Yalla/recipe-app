package com.cgi.nl.recipe.assessment.controller;

import com.cgi.nl.recipe.assessment.service.LogService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/cgi/v1/recipe")
public class LogController {

    private LogService logService;

    @GetMapping(value = "/logs/{logLevel}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Integer>> getFilteredLog(@PathVariable(value = "logLevel") String logLevel) {
        log.info("logLevel value: "+logLevel);
        return ResponseEntity.ok(logService.filterLogFile(logLevel.toUpperCase()));
    }
}
