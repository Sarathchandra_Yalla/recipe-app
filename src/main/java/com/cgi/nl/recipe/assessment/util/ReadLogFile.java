package com.cgi.nl.recipe.assessment.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@Slf4j
public class ReadLogFile {

    public static Map<String, Integer> readLogFile(String logLevel){
        Map<String, Integer> filteredLogMap = new HashMap();
        try {
            File resource = new ClassPathResource("logFile-2018-09-10.log").getFile();

            Stream<String> stream = Files.lines(Paths.get(resource.getPath()));
            stream.filter(record -> record.contains(logLevel))
                    .forEach(logRecord -> sortedLog(filteredLogMap, logRecord));
        } catch (IOException e) {
           log.error("Error while processing log file: "+e.getMessage());
        }
        return filteredLogMap;
    }

    private static void sortedLog(Map<String, Integer> filteredLogMap, String logRecord) {
        if (logRecord != null && logRecord.length() > 24) {
            String desc = logRecord.substring(24, logRecord.length());
            if (filteredLogMap.containsKey(desc)) {
                filteredLogMap.put(desc, filteredLogMap.get(desc) + 1);
            } else {
                filteredLogMap.put(desc, new Integer(1));
            }
        }
    }
}
