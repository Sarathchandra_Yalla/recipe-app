package com.cgi.nl.recipe.assessment.service;

import com.cgi.nl.recipe.assessment.domain.Recipe;

import java.util.List;

public interface RecipeService {
    List<Recipe> getAllRecipes();
    List<Recipe> getAllIngredients(String[] listOfIngredients);
}
