package com.cgi.nl.recipe.assessment.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Recipe implements Serializable {
    private String title;
    private String href;
    private List<String> ingredients;
    private String thumbnail;
}
