var recipeApp = angular.module('recipeApp', []);

recipeApp.controller('RecipeController', ['$scope', 'RecipeService', function ($scope, RecipeService) {

    $scope.getRecipeByIngredients = function () {
        var ingredients = $scope.data.multipleSelect;
        RecipeService.getRecipeByIngredients(ingredients)
            .then(function success(response) {
                    $scope.recipes = response.data;
                    $scope.message = '';
                    $scope.errorMessage = '';
                },
                function error(response) {
                    $scope.message = '';
                    if (response.status === 404) {
                        $scope.errorMessage = 'Recipe not found based on the ingredients!';
                    } else {
                        $scope.errorMessage = "Error while getting Recipe!";
                    }
                });
    }

    $scope.getAllRecipes = function () {
        RecipeService.getAllRecipes()
            .then(function success(response) {
                    $scope.recipes = response.data;
                    $scope.message = '';
                    $scope.errorMessage = '';
                },
                function error(response) {
                    $scope.message = '';
                    $scope.errorMessage = 'Error while getting recipes!';
                });
    }

    $scope.getAllLogsByFilter = function () {
        var logLevel = $scope.logLevel;
        RecipeService.getAllLogsByFilter(logLevel)
            .then(function success(response) {
                    $scope.logs = response.data;
                    $scope.message = '';
                    $scope.errorMessage = '';
                },
                function error(response) {
                    $scope.message = '';
                    $scope.errorMessage = 'Error while getting logs!';
                });
    }
}]);

recipeApp.service('RecipeService', ['$http', function ($http) {

    this.getRecipeByIngredients = function getRecipeByIngredients(ingredients) {
        return $http({
            method: 'GET',
            url: 'api/cgi/v1/recipe/ingredients/' + ingredients
        });
    }

    this.getAllRecipes = function getAllRecipes() {
        return $http({
            method: 'GET',
            url: 'api/cgi/v1/recipe/all'
        });
    }

    this.getAllLogsByFilter = function getAllLogsByFilter(logLevel) {
        return $http({
            method: 'GET',
            url: 'api/cgi/v1/recipe/logs/' + logLevel
        });
    }
}]);
