package com.cgi.nl.recipe.assessment.service;

import com.cgi.nl.recipe.assessment.domain.Recipe;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RecipeServiceImplTest {

    @Autowired
    private RecipeServiceImpl recipeService;

    @Test
    public void testGetAllRecipes(){
        List<Recipe> recipes = recipeService.getAllRecipes();
        assertNotNull(recipes);
        assertTrue(!recipes.isEmpty());
    }

    @Test
    public void testGetAllRecipesWithIngredients(){
        String[] ingredients = {"onions"};
        List<Recipe> recipes = recipeService.getAllIngredients(ingredients);
        assertNotNull(recipes);
        assertTrue(!recipes.isEmpty());
        assertTrue(recipes.size() > 0);
        assertEquals(11, recipes.size());
    }

    @Test
    public void testGetAllRecipesWithMultipleIngredients(){
        String[] ingredients = {"garlic", "olive oil"};
        List<Recipe> recipes = recipeService.getAllIngredients(ingredients);
        assertNotNull(recipes);
        assertTrue(!recipes.isEmpty());
        assertTrue(recipes.size() > 0);
        assertEquals(3, recipes.size());
    }

    @Test
    public void testGetAllRecipesWithInvalidIngredients(){
        String[] ingredients = {"onion", "rice"};
        List<Recipe> recipes = recipeService.getAllIngredients(ingredients);
        assertNotNull(recipes);
        assertTrue(recipes.isEmpty());
    }
}
