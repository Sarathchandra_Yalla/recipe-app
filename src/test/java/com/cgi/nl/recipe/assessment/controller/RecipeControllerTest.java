package com.cgi.nl.recipe.assessment.controller;

import com.cgi.nl.recipe.assessment.domain.Recipe;
import com.cgi.nl.recipe.assessment.service.RecipeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@AutoConfigureMockMvc
@SpringBootTest
public class RecipeControllerTest {

    @Autowired
    private MockMvc mockmvc;
    @MockBean
    private RecipeService recipeService;

    private List<Recipe> recipes;

    @Test
    public void testGetAllRecipes() throws Exception {
        when(recipeService.getAllRecipes()).thenReturn(getRecipes());
        MvcResult mvcResult = mockmvc.perform(MockMvcRequestBuilders.get("/api/cgi/v1/recipe/all")
        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        assertNotNull(mvcResult.getResponse().getContentAsString());
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Portobello Mushroom Sauce"));
        assertTrue(mvcResult.getResponse().getContentAsString().contains("Green Bean Casserole"));
    }

    @Test
    public void testGetAllIngredients() throws Exception {
        String[] ingredient = {"onion"};
        when(recipeService.getAllIngredients(ingredient)).thenReturn(getRecipes());
        MvcResult mvcResult = mockmvc.perform(MockMvcRequestBuilders.get("/api/cgi/v1/recipe/ingredients/onion")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        assertNotNull(mvcResult.getResponse().getContentAsString());
        assertTrue(mvcResult.getResponse().getContentAsString().contains("onion"));
        assertTrue(mvcResult.getResponse().getContentAsString().contains("mushrooms"));
    }

    private List<Recipe> getRecipes(){
        recipes = new ArrayList<>();
        List<String> ingredient1 = new ArrayList<>();
        ingredient1.add("onion");
        ingredient1.add("heavy cream");
        ingredient1.add("mushrooms");
        ingredient1.add("port wine");

        Recipe recipe1 = new Recipe();
        recipe1.setTitle("Portobello Mushroom Sauce");
        recipe1.setIngredients(ingredient1);
        recipe1.setThumbnail("http://img.recipepuppy.com/24124.jpg");
        recipes.add(recipe1);

        List<String> ingredient2 = new ArrayList<>();
        ingredient2.add("onion");
        ingredient2.add("salt");

        Recipe recipe2 = new Recipe();
        recipe2.setTitle("Green Bean Casserole");
        recipe2.setIngredients(ingredient2);
        recipe2.setThumbnail("http://img.recipepuppy.com/707237.jpg");
        recipes.add(recipe2);
        return recipes;
    }
}
