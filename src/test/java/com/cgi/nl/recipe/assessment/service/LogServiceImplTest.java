package com.cgi.nl.recipe.assessment.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LogServiceImplTest {

    @Autowired
    private LogService logService;

    @Test
    public void testFilterLogFileForInfo(){
        Map<String, Integer> logMap = logService.filterLogFile("INFO");
        assertNotNull(logMap);
    }

    @Test
    public void testFilterLogFileForError(){
        Map<String, Integer> logMap = logService.filterLogFile("ERROR");
        assertNotNull(logMap);
    }

    @Test
    public void testFilterLogFileForInvalidInput(){
        Map<String, Integer> logMap = logService.filterLogFile("random");
        assertTrue(logMap.isEmpty());
    }
}
