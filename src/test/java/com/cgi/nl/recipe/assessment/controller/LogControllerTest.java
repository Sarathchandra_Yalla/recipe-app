package com.cgi.nl.recipe.assessment.controller;

import com.cgi.nl.recipe.assessment.service.LogService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@AutoConfigureMockMvc
@SpringBootTest
public class LogControllerTest {

    @Autowired
    private MockMvc mockmvc;
    @MockBean
    private LogService logService;

    @Test
    public void testGetFilteredLog() throws Exception {
        Map<String, Integer> logMap = new HashMap<>();
        logMap.put("LifecycleService:", new Integer(12));
        logMap.put("initializer:", new Integer(5));
        logMap.put("Application:", new Integer(2));
        when(logService.filterLogFile(Mockito.anyString())).thenReturn(logMap);
        MvcResult mvcResult = mockmvc.perform(MockMvcRequestBuilders.get("/api/cgi/v1/recipe/logs/info")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        System.out.println("mvcResult "+mvcResult.getResponse().getContentAsString());
        assertNotNull(mvcResult.getResponse().getContentAsString());
        assertTrue(mvcResult.getResponse().getContentAsString().contains("LifecycleService:"));
        assertTrue(mvcResult.getResponse().getContentAsString().contains("initializer"));
    }

    @Test
    public void testGetFilteredLogForInvalidFilter() throws Exception {
        when(logService.filterLogFile(Mockito.anyString())).thenReturn(Collections.EMPTY_MAP);
        MvcResult mvcResult = mockmvc.perform(MockMvcRequestBuilders.get("/api/cgi/v1/recipe/logs/test")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        System.out.println("mvcResult "+mvcResult.getResponse().getContentAsString());
        assertNotNull(mvcResult.getResponse().getContentAsString());
    }
}
