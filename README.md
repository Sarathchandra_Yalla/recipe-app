# recipe-app
The recipe app has different end points to fetch all recipes / fetch the recipes related to the selected ingredients
and also which will provide the log analyser based on the selected log level.

Service URL
--
 - http://localhost:8080/index.html

Swagger URL
--
 - Below Swagger Documentation can help to understand about the interface
 - http://localhost:8080/swagger-ui.html

Technical Info
--
Softwares:
######
 - Spring Boot 2.3.3 (JDK 8)
 - Maven 3.6.1
 - Swagger 3.0.0
 - AngularJS

Deployment & Run
--
Manual:
######
 - Clone the repository from Bitbucket 'git clone https://Sarathchandra_Yalla@bitbucket.org/Sarathchandra_Yalla/recipe-app.git'
 - mvn clean install
 - Run the spring boot app from target 'java -jar recipe-app-0.0.1-SNAPSHOT.jar'
